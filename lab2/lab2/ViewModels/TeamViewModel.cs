﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.ViewModels
{
    public class TeamViewModel: ViewModelBase
    {
        int _id;
        public int Id { get=> _id;  set { _id = value;OnPropertyChanged("Id"); } }
        
        string _name;
        public string Name { get=> _name;  set { _name = value; OnPropertyChanged("Name"); } }

        int _currentProjectId;
        public int CurrentProjectId { get => _currentProjectId;  
            set {
                _currentProjectId = value;
                OnPropertyChanged("CurrentProjectId");
            } }

        ObservableCollection<UserViewModel> _users;
        public ObservableCollection<UserViewModel> Users { get => _users;  
            set {
                _users = value;
                OnPropertyChanged("Users");
            } }
    }
}
