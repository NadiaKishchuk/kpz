﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.ViewModels
{
    public class UserViewModel: ViewModelBase
    {
        int _id;
        public int Id { get=> _id; set { _id = value; OnPropertyChanged("Id");  } }
       
        string _name;
        public string Name {
            get=> _name;
             set {
                _name = value; OnPropertyChanged("Name");
            } }

        string _surname;
        public string Surname
        {
            get => _surname;
             set
            {
                _surname = value; OnPropertyChanged("Surname");
            }
        }

        string _skills;
        public string Skills
        {
            get => _skills;
             set
            {
                _skills = value; OnPropertyChanged("Skills");
            }
        }

        float _costPerHour;
        public float CostPerHour
        {
            get => _costPerHour;
             set
            {
                _costPerHour = value; OnPropertyChanged("CostPerHour");
            }
        }

        string _education;
        public string Education
        {
            get => _education;
             set
            {
                _education = value; OnPropertyChanged("Education");
            }
        }

        int? _teamId;
        public int? TeamId
        {
            get => _teamId;
             set
            {
                _teamId = value; OnPropertyChanged("TeamId");
            }
        }

        Role? _role;
        public Role? Role
        {
            get => _role;
             set
            {
                _role = value; OnPropertyChanged("Role");
            }
        }
        ProjectTask _curentTask;
        public ProjectTask CurentTask
        {
            get => _curentTask;
             set
            {
                _curentTask = value; OnPropertyChanged("CurentTask");
            }
        }

        ObservableCollection<Project> _projects;

        public ObservableCollection<Project> Projects
        {
            get => _projects;
             set
            {
                _projects = value; OnPropertyChanged("Projects");
            }
        }

    }
}
