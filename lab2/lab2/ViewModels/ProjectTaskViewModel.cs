﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TaskStatus = System.Threading.Tasks.TaskStatus;

namespace lab2.ViewModels
{
    public class ProjectTaskViewModel : ViewModelBase
    {
        int _id;
        public int Id
        {
            get => _id;
             set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }

        int _idProject;
        public int IdProject
        {
            get => _idProject;
             set
            {
                _idProject = value;
                OnPropertyChanged("IdProject");
            }
        }
        int _idDeveloper;
        public int IdDeveloper
        {
            get => _idDeveloper;
             set
            {
                _idDeveloper = value;
                OnPropertyChanged("IdDeveloper");
            }
        }
        string _description;
        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }
        DateTime _deadline;
        public DateTime Deadline
        {
            get => _deadline;
            set
            {
                _deadline = value;
                OnPropertyChanged("Deadline");
            }
        }
        ProjectTaskStatus _status;
        public ProjectTaskStatus Status
        {
            get => _status;
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }
    }
}
