﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace lab2.ViewModels
{
    public class ProjectViewModel: ViewModelBase
    {
        private int _id;
        public int Id { get => _id;  set { _id = value; OnPropertyChanged("Id"); } }

        string _name;
        public string Name { get => _name;  set { _name = value; OnPropertyChanged("Name"); } }

        string _desription;
        public string Description { get => _desription;
             set { _desription = value; OnPropertyChanged("Description"); } }

        int _teamId;
        public int TeamId { get => _teamId;  set { _teamId = value; OnPropertyChanged("TeamId"); } }

        ProjectStatus _status;
        public ProjectStatus Status { get => _status;
             set { _status = value; OnPropertyChanged("Status"); } }

        string _customer;
        public string Customer { get => _customer;
             set { _customer = value; OnPropertyChanged("Customer"); } }
    }
}
