﻿using lab2.Commands;
using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.ViewModels
{
     public class UserParametersBase: ViewModelBase
    {
        protected string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        protected string _surname;
        public string Surname
        {
            get => _surname;
            set
            {
                _surname = value;
                OnPropertyChanged("Surname");
            }
        }
    }
    public class UserParameters: UserParametersBase
    {
        
        string _skills;
        public string Skills
        {
            get => _skills;
            set
            {
                _skills = value;
                OnPropertyChanged("Skills");
            }
        }
        float _costPerHour;
        public float CostPerHour
        {
            get => _costPerHour;
            set
            {
                _costPerHour = value;
                OnPropertyChanged("CostPerHour");
            }
        }
        string _education;
        public string Education
        {
            get => _education;
            set
            {
                _education = value;
                OnPropertyChanged("Education");
            }
        }
        Role? _role;
        public Role? Role
        {
            get => _role;
            set
            {
                _role = value;
                OnPropertyChanged("Role");
            }
        }
    }

    public class DataViewModel: ViewModelBase
    {

        DataViewModel()
        {
            SetWelcomeVisibility = new Command(WelcomeVisibility);
            DeleteSelectedProject = new Command(DeleteProject);
            SetDataVisibility = new Command(DataVisibility);
            RegistrationUser = new Command(ResgisterUser);
            CheckingFullRegistration = new Command(CheckFullRegistration);
            LogingIn = new Command(LogIn);
            ClosingSelectedTask = new Command(CloseSelectedTask);
            UserParamLogIn = new UserParametersBase() { Surname = string.Empty, Name = string.Empty };
            UserParameters = new UserParameters() { Name = "", CostPerHour = 0, Education = "", Role = null, Skills = "", Surname = "" };
        }
        string isCheckedFullRegistration = "False";
        public string IsCheckedFullRegistration
        {
            get => isCheckedFullRegistration;
            set
            {
                isCheckedFullRegistration = value;
                OnPropertyChanged("IsCheckedFullRegistration");
            }
        }
        string _visibilityFullRegistration = "Hidden";
        public string VisibilityFullRegistration
        {
            get => _visibilityFullRegistration;
            set
            {
                _visibilityFullRegistration = value;
                OnPropertyChanged("VisibilityFullRegistration");
            }
        }
        
        public Command CheckingFullRegistration { get; set; }
        private void CheckFullRegistration( object arg)
        {
            if (VisibilityFullRegistration == "Hidden")
                VisibilityFullRegistration = "Visible";
            else
                VisibilityFullRegistration = "Hidden";

        }
        public string VisibilityWelcomeControl { get=>_visibilityWelcomeControl;
            set
            {
                _visibilityWelcomeControl = value;
                OnPropertyChanged("VisibilityWelcomeControl");
            }
        }
        string _visibilityWelcomeControl = "Visible";
        string _visibilityDataControl = "Hidden";
        public string VisibilityDataControl
        {
            get => _visibilityDataControl;
            set
            {
                _visibilityDataControl = value;
                OnPropertyChanged("VisibilityDataControl");
            }
        } 
        public void ResgisterUser(object args)
        {
            if( Users.Count(user=> user.Name == UserParameters.Name
            && user.Surname == UserParameters.Surname)!=0)
                return;
            Users.Add(new UserViewModel()
            {
                CostPerHour = UserParameters.CostPerHour,
                CurentTask = null,
                Education = UserParameters.Education,
                Id = Users.Max(x => x.Id) + 1,
                Name = UserParameters.Name,
                Projects = new ObservableCollection<Project>(),
                Role = UserParameters.Role,
                Skills = UserParameters.Skills,
                Surname = UserParameters.Surname,
                TeamId = null
            });
            VisibilityWelcomeControl = "Hidden";
            VisibilityDataControl = "Visible";
        }
        void LogIn(object args)
        {
            var user = Users.FirstOrDefault(x=>x.Name== UserParamLogIn.Name);
            if(user==null)
                return;
            if (user.Surname == UserParamLogIn.Surname)
            {
                VisibilityWelcomeControl = "Hidden";
                VisibilityDataControl = "Visible";
            }
            

        }
        public Command LogingIn { get; set; }
        public Command RegistrationUser { get; set; }
        UserParametersBase _userParamLogIn;
        public UserParametersBase UserParamLogIn
        {
            get => _userParamLogIn;
            set
            {
                _userParamLogIn = value;
                OnPropertyChanged("UserParamLogIn");
            }
        }
        UserParameters _userParameters;
        public UserParameters UserParameters {
            get => _userParameters;
            set
            {
                _userParameters = value;
                OnPropertyChanged("UserParameters");
            }
        }
        private  string _visibilityAutorization= "Register";
        public string VisibilityAutorization
        {
            get => _visibilityAutorization;
            set
            {
                _visibilityAutorization = value;
                OnPropertyChanged("VisibilityAutorization");
            }
        }
        private string _visibilityMain = "UsersControl";
        public string VisibilityMain
        {
            get => _visibilityMain;
            set
            {
                _visibilityMain = value;
                OnPropertyChanged("VisibilityMain");
            }
        }
        private string _visibilityData = "UsersControl";
        public string VisibilityData
        {
            get => _visibilityData;
            set
            {
                _visibilityData = value;
                OnPropertyChanged("VisibilityData");
            }
        }
        public Command SetDataVisibility { get; set; }
        void DataVisibility(object args)
        {
            VisibilityData = args.ToString();
        }

        public Command ClosingSelectedTask { get; private set; }
        void CloseSelectedTask(object args)
        {
           SelectedTask.Status = ProjectTaskStatus.Done;
        }
        ProjectTaskViewModel _selectedTask;
        public ProjectTaskViewModel SelectedTask
        {
            get => _selectedTask;
            set
            {
                _selectedTask = value;
                OnPropertyChanged("SelectedTask");
            }
        }

        public Command DeleteSelectedProject { get; private set; }
        void DeleteProject(object args)
        {
            Projects.Remove(SelectedProject);
        }
        ProjectViewModel _selectedProject;
        public ProjectViewModel SelectedProject
        {
            get => _selectedProject;
            set
            {
                _selectedProject = value;
                OnPropertyChanged("SelectedProject");
            }
        }
       
        public Command SetWelcomeVisibility { get; set; }
        void WelcomeVisibility(object args)
        {
            VisibilityAutorization = args.ToString();
        }
        ObservableCollection<ProjectViewModel> _projects;
        public ObservableCollection<ProjectViewModel> Projects 
        { get=> _projects;
            set {
                _projects = value;
                OnPropertyChanged("Projects");
            } }
        ObservableCollection<ProjectTaskViewModel> _projectTasks;
        public ObservableCollection<ProjectTaskViewModel> ProjectTasks
        {
            get => _projectTasks;
            set
            {
                _projectTasks = value;
                OnPropertyChanged("ProjectTasks");
            }
        }
        ObservableCollection<TeamViewModel> _teams;
        public ObservableCollection<TeamViewModel> Teams
        {
            get => _teams;
            set
            {
                _teams = value;
                OnPropertyChanged("Teams");
            }
        }
        ObservableCollection<UserViewModel> _users;
        public ObservableCollection<UserViewModel> Users
        {
            get => _users;
            set
            {
                _users = value;
                OnPropertyChanged("Users");
            }
        }

    }
}
