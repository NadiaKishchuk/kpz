﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace lab2.Converters
{
    public class StatusConverterToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum status;
            if (value is ProjectStatus)
            {
                status = (ProjectStatus)value;
            }
            else
            {
                status = (ProjectTaskStatus)value;
            }
            return status.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
