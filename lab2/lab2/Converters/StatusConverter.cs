﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using ProjectTaskStatus = lab2.Entites.ProjectTaskStatus;

namespace lab2.Converters
{
    public class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum status;
            if (value is ProjectStatus)
            {
                status = (ProjectStatus)value;
            }    
            else {
                status = (ProjectTaskStatus)value;
            }
                  
            var uri = new Uri(String.Format(@"../../Images/{1}/{0}.png", status, parameter),UriKind.Relative);
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
