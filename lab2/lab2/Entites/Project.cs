﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Entites
{
    [DataContract]
    public enum ProjectStatus
    {
        [EnumMember]
        Analizing,
        [EnumMember]
        Designing,
        [EnumMember]
        InProcess,
        [EnumMember]
        Finished


    }
    [DataContract]
    public class Project
    {
        [DataMember]
        public  int Id{ get; private set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Description { get;  set; }
        [DataMember]
        public int TeamId { get; private set; }
        [DataMember]
        public ProjectStatus Status { get;  set; }
        [DataMember]
        public  string Customer { get; private set; }

        public Project(int id, string name, string description, int teamId, string customer,
            ProjectStatus status = ProjectStatus.Analizing)
        {
            Id = id;
            Name = name;
            Description = description;
            TeamId = teamId;
            Customer = customer;
            Status = status;
        }
        public Project() { }


    }
}
