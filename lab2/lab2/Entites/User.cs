﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Entites
{
    [DataContract]
    public enum Role
    {
        [EnumMember]
        Head,
        [EnumMember]
        Manager,
        [EnumMember]
        HR,
        [EnumMember]
        Developer,
        [EnumMember]
        Recruiter
    }
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Surname { get; private set; }
        [DataMember]
        public string Skills { get; private set; }
        [DataMember]
        public float CostPerHour { get; private set; }
        [DataMember]
        public string Education { get; private set; }
        [DataMember]
        public int TeamId { get; private set; }
        [DataMember]
        public Role Role { get; private set; }
        [DataMember]
        public ProjectTask CurentTask { get; private set; }
        [DataMember]
        public IEnumerable<Project> Projects { get; private set; }

        public User(int id, string name, string surname, string skills, float costPerHour,
            string education, int teamId, Role role, ProjectTask curentTask,
            IEnumerable<Project> projects)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Skills = skills;
            CostPerHour = costPerHour;
            Education = education;
            TeamId = teamId;
            Role = role;
            CurentTask = curentTask;
            Projects = projects;
        }
        public User()
        {
            Projects = new List<Project>();
        }

    }
}
