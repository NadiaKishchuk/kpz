﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Entites
{
    [DataContract]
    public class Team
    {
        [DataMember]
        public int Id { get; private set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public int CurrentProjectId { get; private set;}
        [DataMember]
        public IEnumerable<User> Users { get; private set; }

        public Team (int id, string name, int currentProjId, IEnumerable<User> users)
        {
            Id = id;
            Name = name;
            CurrentProjectId = currentProjId;
            Users = users;

        }
        public Team()
        {
            Users = new List<User>();
        }
    }
}
