﻿using lab2.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Entites
{
    [DataContract]
    public class DataModel
    {
        [DataMember]
        public IEnumerable<Project> Projects { get;  set; }
        [DataMember]
        public IEnumerable<ProjectTask> ProjectTasks { get; private set; }
        [DataMember]
        public IEnumerable<Team> Teams { get; private set; }
        [DataMember]
        public IEnumerable<User> Users { get; private set; }

        public DataModel(IEnumerable<Project> projects, IEnumerable<ProjectTask> projectTasks,
            IEnumerable<Team> teams, IEnumerable<User> users)
        {
            Projects = projects;
            ProjectTasks = projectTasks;
            Teams = teams;
            Users = users;
        }
        public DataModel()
        {
            Projects = new List<Project>();
            ProjectTasks = new List<ProjectTask>();
            Teams = new List<Team>();
            Users = new List<User>();
        }
        public static string _path = "serializedFile.dat";
        public static DataModel Load()
        {
            if (File.Exists(_path))
            {
                return DataSerializer.Deserialize(_path);
            }
            return new DataModel();
        }
        public void Save()
        {
            DataSerializer.Serialize(_path,this);
        }

    }
}
