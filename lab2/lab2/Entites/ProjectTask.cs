﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Entites
{
    [DataContract]
    public enum ProjectTaskStatus
    {
        [EnumMember]
        Open,
        [EnumMember]
        InProgress,
        [EnumMember]
        InReview,
        [EnumMember]
        Done

    }
    [DataContract]
    public class ProjectTask
    {
        [DataMember]
        readonly public int Id;
        [DataMember]
        public int IdProject{ get; private set; }
    [DataMember]
        public int IdDeveloper { get; private set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime Deadline { get; set; }
        [DataMember]
        public ProjectTaskStatus Status { get; set; }

        public ProjectTask( int id, int idProject, int idDeveloper, string description,
            DateTime deadline, ProjectTaskStatus status = ProjectTaskStatus.Open)
        {
            Id = id;
            IdProject = idProject;
            IdDeveloper = idDeveloper;
            Description = description;
            Deadline = deadline;
            Status = status;
        }
        public ProjectTask() { }
    }
}
