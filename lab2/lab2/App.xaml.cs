﻿using AutoMapper;
using lab2.Base;
using lab2.Entites;
using lab2.ViewModels;
using System;
using lab2.Pages;
using System.Windows;
using lab2.UserContols;
using System.Linq;

namespace lab2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// 
    
    public partial class App : Application
    {
        DataModel _dataModel;
        DataViewModel _dataViewModel;
        public App()
        {
            
            _dataModel = DataModel.Load();
            
            new Mapping().Create();
            Mapper.AllowNullDestinationValues = true;
            _dataViewModel = Mapper.Map<DataModel, DataViewModel>(_dataModel);
           
            var window = new MainWindow() { DataContext = _dataViewModel };
          //  window.MainFrame.Content = new lab2.Pages.Welcome() { DataContext=_dataViewModel};
            window.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                _dataModel = Mapper.Map<DataViewModel, DataModel>(_dataViewModel);
                _dataModel.Save();
            }
            catch( Exception)
            {
                base.OnExit(e);
            }
        }

    }
}
