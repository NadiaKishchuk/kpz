﻿using lab2.Entites;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Serialization
{
    public class DataSerializer
    {
        public static void Serialize(string filename, DataModel dataModel)
        {
            var formatter = new DataContractSerializer(typeof(DataModel));
            var s = new FileStream(filename, FileMode.Create);
            formatter.WriteObject(s, dataModel);
            s.Close();
        }

        public static DataModel Deserialize( string filename)
        {
            var s = new FileStream(filename, FileMode.Open);
            var formatter = new DataContractSerializer(typeof(DataModel));
            return (DataModel)formatter.ReadObject(s);
        }
    }
}
