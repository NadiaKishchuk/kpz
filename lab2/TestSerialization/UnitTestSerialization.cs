﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using lab2.Entites;
using lab2.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace TestSerialization
{
    [TestClass]
    public class UnitTestSerialization
    {
        [TestMethod]
        public void TestMethodSerialize()
        {
            List<ProjectTask> tasks = new List<ProjectTask>()
            { new ProjectTask(1,1,1,"create func1",new DateTime(2022,9,30)),
              new ProjectTask(1,1,2,"Analyze",new DateTime(2022,9,30)),
            new ProjectTask(1,1,3,"create func2",new DateTime(2022,9,30))
            };
            
            Project pr = new Project(1, "projectName1", "no desc", 1, "Pol");
            Project pr2 = new Project(2, "projectName2", "no desc", 2, "Pol2");
            List<Project> projects = new List<Project>() { pr,pr2 };
            List<User> users = new List<User>() 
            { new User(1,"Name1","Surname1","skils",200,"NULP",1,Role.Manager,tasks.ElementAt(0),projects),
             new User(2,"Name1","Surname1","skils",200,"NULP",1,Role.Developer,tasks.ElementAt(1),projects),
             new User(3,"Name1","Surname1","skils",200,"NULP",1,Role.Head,tasks.ElementAt(2),projects),

            };
            var users2 = new List<User>(users);
            
            var user2 = new User(4, "Name4", "Surname4", "skils", 200, "NULP", 2, Role.Manager, tasks.ElementAt(0), projects);
            users2.Add(user2);
            Team team = new Team(1, "Cuties", 1, users);
            Team team2 = new Team(2, "Cuties2", 2, new List<User>() { user2 });
            List<Team> teams = new List<Team>() { team,team2};
            
            var model = new DataModel(projects, tasks, teams, users2);

            DataSerializer.Serialize("serializedFile.dat", model);

        }

        [TestMethod]
        public void TestMethodDeserialize()
        {
            var model = DataSerializer.Deserialize("serializedFile.dat");
        }
    }
}
