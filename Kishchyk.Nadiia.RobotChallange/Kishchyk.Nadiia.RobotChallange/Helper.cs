﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
namespace Kishchyk.Nadiia.RobotChallange
{
    public class Helper
    {
        static int DistanceFromStation = 2;
 
        static int _minEnergyAtStation = 100;
        static int _maxGettingAtStation = 40;
        static int _checkAmountAtStation = 4;

        static int _distanceFromStFreeLineMethoud = 2;

        /// <summary>
        /// return nearest station at x and y line, to trace is free(without robots)
        /// </summary>
        /// <param name="stations"></param>
        /// <param name="robotPos"></param>
        /// <param name="robots"></param>
        /// <returns></returns>
        public static EnergyStation AlmostFreeStAtLine(IList<EnergyStation> stations, Position robotPos, IList<Robot.Common.Robot> robots)
        {
            var StationAtLine = stations.Where(st => st.Position.X >= robotPos.X - _distanceFromStFreeLineMethoud
            && st.Position.X <= robotPos.X + _distanceFromStFreeLineMethoud);

            Dictionary<EnergyStation, int> EnergyStations = new Dictionary<EnergyStation, int>();
            IEnumerable<Robot.Common.Robot> robotsAtSt;
            Position nearStPos;
            int amountAtStation;
            if (StationAtLine.Count() != 0)
            {
                foreach (var station in StationAtLine)
                {
                    if ((station.Position.X - DistanceFromStation >= robotPos.X && station.Position.X + DistanceFromStation <= robotPos.X)
                        &&
                        (station.Position.Y - DistanceFromStation >= robotPos.Y && station.Position.Y + DistanceFromStation <= robotPos.Y))
                        continue;

                    nearStPos = NearestFreeCellAtStation(station.Position, robotPos, robots);
                    amountAtStation = AmountRobotsAtStation(robots, station, out robotsAtSt);
                    if (FreeLineY(robotPos.Y, nearStPos.Y, robotPos.X, robots)
                        && (amountAtStation <= _checkAmountAtStation||_maxGettingAtStation*2<station.Energy))
                    {
                        if (!EnergyStations.ContainsKey(station))
                            EnergyStations.Add(station, DistanceToCell(station.Position, robotPos));
                    }
                }
            }
            StationAtLine = stations.Where(st => st.Position.Y >= robotPos.Y - _distanceFromStFreeLineMethoud
            && st.Position.Y <= robotPos.Y + _distanceFromStFreeLineMethoud);

            if (StationAtLine.Count() != 0)
            {
                foreach (var station in StationAtLine)
                {
                    if ((station.Position.X - DistanceFromStation >= robotPos.X && station.Position.X + DistanceFromStation <= robotPos.X)
                        &&
                        (station.Position.Y - DistanceFromStation >= robotPos.Y && station.Position.Y + DistanceFromStation <= robotPos.Y))
                        continue;
                    nearStPos = NearestFreeCellAtStation(station.Position, robotPos, robots);
                    amountAtStation = AmountRobotsAtStation(robots, station, out robotsAtSt);
                    if (FreeLineX(robotPos.X, nearStPos.X, robotPos.Y, robots)
                          && (amountAtStation <= _checkAmountAtStation || _maxGettingAtStation*2 < station.Energy))
                    {
                        if (!EnergyStations.ContainsKey(station))
                            EnergyStations.Add(station, DistanceToCell(station.Position, robotPos));
                    }
                }
            }
            var sorted = EnergyStations.OrderBy(keyValue => keyValue.Value);
            return sorted.FirstOrDefault().Key;
        }
        public static MoveCommand Moving(IList<EnergyStation> stations, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots)
        {
            var funcPos = NextMove(stations, robot, robots, out _);

            robot.Position.X = funcPos.X;
            robot.Position.Y = funcPos.Y;
            return new MoveCommand() { NewPosition = robot.Position };
        }
        public static Position NextMove(IList<EnergyStation> stations, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, out bool WithAtack)
        {
            var NearestStations = NearestFreeStation(stations, robot.Position, robots);
            Position nextStep = new Position(robot.Position.X + 1, robot.Position.Y + 1);
            WithAtack = false;
            
            foreach (var st in NearestStations)
            {
             
                nextStep = NearestFreeCellAtStation(st.Position, robot.Position, robots);
                if (nextStep != null && DistanceToCell(nextStep, robot.Position) < robot.Energy
                    &&st.Energy > _minEnergyAtStation)
                    return nextStep;
            }
            
            WithAtack = true;
            return NearRobotFreeCell(stations,robot.Position,robots);
        }
        static public Position NearRobotFreeCell(IEnumerable<EnergyStation> stations,Position robotPosition, IList<Robot.Common.Robot> robots)
        {
            Position nextStep =new Position(robotPosition.X + 1, robotPosition.Y + 1);
            foreach (var st in stations)
            {
                if (robotPosition.X > st.Position.X)
                {
                    nextStep = new Position(robotPosition.X - 1, robotPosition.Y);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }

                if (robotPosition.X < st.Position.X)
                {
                    nextStep = new Position(robotPosition.X + 1, robotPosition.Y);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if (robotPosition.Y > st.Position.Y)
                {
                    nextStep = new Position(robotPosition.X, robotPosition.Y - 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if (robotPosition.Y < st.Position.Y)
                {
                    nextStep = new Position(robotPosition.X, robotPosition.Y + 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if(robotPosition.Y <= st.Position.Y&&robotPosition.X<= st.Position.X)
                {
                    nextStep = new Position(robotPosition.X+1, robotPosition.Y + 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if (robotPosition.Y <= st.Position.Y && robotPosition.X >= st.Position.X)
                {
                    nextStep = new Position(robotPosition.X - 1, robotPosition.Y + 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if (robotPosition.Y >= st.Position.Y && robotPosition.X >= st.Position.X)
                {
                    nextStep = new Position(robotPosition.X - 1, robotPosition.Y - 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
                if (robotPosition.Y >= st.Position.Y && robotPosition.X <= st.Position.X)
                {
                    nextStep = new Position(robotPosition.X + 1, robotPosition.Y - 1);
                    if (IsCellFree(nextStep, robots))
                        return nextStep;
                }
            }
            
            return nextStep;
        }
        public static bool IsStationCompletlyFree(EnergyStation station, IList<Robot.Common.Robot> robots)
        {
            return AmountRobotsAtStation(robots, station, out _) == 0;
        }
        public static bool IsCellFree(Position cell, IEnumerable<Robot.Common.Robot> robots)
        {
            return robots.FirstOrDefault(rb => rb.Position.X == cell.X && rb.Position.Y == cell.Y) == null;
        }
        public static Position NearestFreeCellAtStation(Position stationPosition, Position robotPosition, IList<Robot.Common.Robot> robots)
        {
            int distance = int.MaxValue, tempDistance;
            Position neerestCell = stationPosition;
            bool changed = false;
            var robotsAtStation = robots.Where(robot =>
            (robot.Position.X >= stationPosition.X - DistanceFromStation && robot.Position.X <= stationPosition.X + DistanceFromStation)
            &&
            (robot.Position.Y >= stationPosition.Y - DistanceFromStation && robot.Position.Y <= stationPosition.Y + DistanceFromStation));

            for (int i = stationPosition.X - DistanceFromStation; i <= stationPosition.X + DistanceFromStation; i++)
            {
                for (int j = stationPosition.Y - DistanceFromStation; j <= stationPosition.Y + DistanceFromStation; j++)
                {
                    if (robotPosition.X == i && robotPosition.Y == j)
                        continue;
                    if (IsCellFree(new Position(i,j),robotsAtStation))
                    {
                        tempDistance = DistanceToCell(new Position(i, j), robotPosition);
                        if (distance > tempDistance)
                        {
                            neerestCell = new Position(i, j);
                            distance = tempDistance;
                            changed = true;
                        }

                    }
                }
            }
            return changed?neerestCell:null;
        }
        public static bool FreeLineX(int startPointX, int endPointX, int lineIndexY, IList<Robot.Common.Robot> robots)
        {
            if (startPointX > endPointX)
            {
                int temp = startPointX - 1;
                startPointX = endPointX;
                endPointX = temp;
            }
            else
                startPointX++;
            for (int i = startPointX; i <= endPointX; i++)
            {
                if (robots.FirstOrDefault(robot => robot.Position.X == i && robot.Position.Y == lineIndexY) != null)
                    return false;
            }
            return true;
        }
        public static bool FreeLineY(int startPointY, int endPointY, int lineIndexX, IList<Robot.Common.Robot> robots)
        {
            if (startPointY > endPointY){
                int temp = startPointY - 1;
                startPointY = endPointY;
                endPointY = temp;
            }else
                startPointY++;
            for (int i = startPointY; i <= endPointY; i++){
                if (robots.FirstOrDefault(robot => robot.Position.Y == i && robot.Position.X == lineIndexX) != null)
                    return false;
            }
            return true;
        }
        public static bool IsAtStation(Position robotPosition, IList<EnergyStation> stations, out EnergyStation StationWithRobot)
        {
            StationWithRobot = stations.Where(
                station => (robotPosition.X >= station.Position.X - DistanceFromStation&& robotPosition.X <= station.Position.X + DistanceFromStation)
            && 
            (robotPosition.Y >= station.Position.Y - DistanceFromStation && robotPosition.Y <= station.Position.Y + DistanceFromStation))
                ?.OrderByDescending(st=>st.Energy).FirstOrDefault();

            return StationWithRobot != null;
        }
        public static IEnumerable<EnergyStation> NearestFreeStation(IList<EnergyStation> stations, Position robotPosition, IList<Robot.Common.Robot> robots)
        {
            Dictionary<EnergyStation, int> freeStations = new Dictionary<EnergyStation, int>();
            int distance, amountRobotsAtSt;
            EnergyStation energyStation;
            foreach (var station in stations)
            {
                if ((robotPosition.X >= station.Position.X - DistanceFromStation && robotPosition.X <= station.Position.X + DistanceFromStation)
                &&
                (robotPosition.Y >= station.Position.Y - DistanceFromStation && robotPosition.Y <= station.Position.Y + DistanceFromStation))
                    continue;
                if (AmountRobotsAtStation(robots, station, out _) <= _checkAmountAtStation||station.Energy>_maxGettingAtStation*7)
                {
                    distance = DistanceToCell(station.Position, robotPosition);

                    freeStations.Add(station, distance);
                }
            }
            
            return freeStations.OrderBy(station => station.Value).ThenByDescending(st => st.Key.Energy)
            .Select(StationDistance => StationDistance.Key);
        }
        public static bool IsStationFree(IList<Robot.Common.Robot> robots, EnergyStation station, out int AmountRobots)
        {
            AmountRobots = AmountRobotsAtStation(robots, station, out _);
            return AmountRobots < (Math.Pow(2 * DistanceFromStation + 1, 2) - 1);
        }

        public static int AmountRobotsAtStation(IList<Robot.Common.Robot> robots, EnergyStation station, out IEnumerable<Robot.Common.Robot> robotsAtSt)
        {
            robotsAtSt = robots
                .Where(robot => (robot.Position.X >= station.Position.X - DistanceFromStation
                && robot.Position.X <= station.Position.X + DistanceFromStation)
            && (robot.Position.Y >= station.Position.Y - DistanceFromStation && robot.Position.Y <= station.Position.Y + DistanceFromStation))
                ;
            return robotsAtSt.Count();
        }

        public static int DistanceToCell(Position stationPosition, Position robotPosition)
        {
            return (int)(Math.Pow(robotPosition.X - stationPosition.X, 2) + Math.Pow(robotPosition.Y - stationPosition.Y, 2));

        }

    }

}
