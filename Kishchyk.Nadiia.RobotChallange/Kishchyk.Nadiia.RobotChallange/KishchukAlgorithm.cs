﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

using System.Diagnostics;
using System.IO;

namespace Kishchyk.Nadiia.RobotChallange
{
    
    public class KishchukAlgorithm : IRobotAlgorithm
    {
        public string Author => "Kishchuk Nadia";

        int AmmountMyRobots = 10;
        static int _maxGettingAtStation = 40;
        int _round = 0; 
        int fri = -1;
        
        

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            
            Robot.Common.Robot currentRobot = robots[robotToMoveIndex];
            EnergyStation energyStation;
            var newRobotPos = currentRobot.Position;
            int amountRobots;
            Position station;
            if (fri == -1)
                fri = robotToMoveIndex;
            if (fri == robotToMoveIndex)
                ++_round;

            if (AmmountMyRobots < 100 && currentRobot.Energy > 300)
            {
                ++AmmountMyRobots;
                return new CreateNewRobotCommand() { NewRobotEnergy = 100 };
            }
            
            if (Helper.IsAtStation(currentRobot.Position, map.Stations, out energyStation) && Helper.IsStationFree(robots, energyStation, out amountRobots))
            {
                if (AmmountMyRobots < 100 && currentRobot.Energy > 280)
                {
                    ++AmmountMyRobots;
                    return new CreateNewRobotCommand() { NewRobotEnergy = 90 };
                }
                if (energyStation.Energy < _maxGettingAtStation  && _round > 1)
                {
                    bool withAtt= false;
                    newRobotPos = Helper.NextMove(map.Stations, currentRobot, robots, out withAtt);
                    
                    if (withAtt&&_round!=28)
                    {
                        energyStation = Helper.AlmostFreeStAtLine(map.Stations, currentRobot.Position, robots);
                        if (energyStation != null
                        && Helper.DistanceToCell(newRobotPos, currentRobot.Position) < currentRobot.Energy)
                            newRobotPos = Helper.NearestFreeCellAtStation(energyStation.Position, currentRobot.Position, robots);
                    }
                   
                     currentRobot.Position.X = newRobotPos.X;
                     currentRobot.Position.Y = newRobotPos.Y;
                     return new MoveCommand() { NewPosition = currentRobot.Position };
                    

                }
                return new CollectEnergyCommand();
              
            } 
            
            return Helper.Moving(map.Stations, currentRobot, robots);
            
           
        }
        
        
       
       
    }
}
