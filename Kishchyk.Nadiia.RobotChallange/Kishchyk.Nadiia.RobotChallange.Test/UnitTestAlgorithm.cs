﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections;
using Kishchyk.Nadiia.RobotChallange;
using System.Collections.Generic;
using System.Linq;
namespace Kishchyk.Nadiia.RobotChallange.Test
{
    [TestClass]
    public class UnitTestAlgorithm
    {
        [TestMethod]
        public void TestDistanceToCell()
        {
            Position posStart = new Position(2, 3), posEnd = new Position(2,4), posEnd2 = new Position(3, 5);
            Assert.AreEqual(1, Helper.DistanceToCell(posStart, posEnd));
            Assert.AreEqual(5, Helper.DistanceToCell(posStart, posEnd2));
        }
        [TestMethod]
        public void TestAmountRobotsAtStation()
        {
            List <Robot.Common.Robot> robots = new List <Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(14,10)});
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 8) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(13, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 13) });
            EnergyStation energyStation = new EnergyStation(){Position = new Position(15,10) };
            Assert.AreEqual(5, Helper.AmountRobotsAtStation(robots, energyStation,out _));
        }
        [TestMethod]
        public void TestIsStationFree()
        {
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(14, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 8) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(13, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 13) });
            EnergyStation energyStation = new EnergyStation() { Position = new Position(15, 10) };
            int amount;
            Assert.AreEqual(true, Helper.IsStationFree(robots, energyStation, out amount));
            Assert.AreEqual(5, amount);
            robots.Clear();
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 13) });
            ;
            Assert.IsTrue(Helper.IsStationFree(robots, energyStation, out amount));
            Assert.AreEqual(0, amount);
        }

        [TestMethod]
        public void TestNearestFreeStation()
        {
            List<EnergyStation> stations = new List<EnergyStation>();
            EnergyStation nearest = new EnergyStation() { Position = new Position(18, 9) },
                secondNearest = new EnergyStation() { Position = new Position(15, 10) };
            stations.Add(secondNearest);
            stations.Add(nearest);
            stations.Add(new EnergyStation() { Position = new Position(20, 12) });
            stations.Add(new EnergyStation() { Position = new Position(18, 15) });

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(14,10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 15) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 15) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(19, 15) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18, 16) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 14) });
           
            var nearestStationReturned = Helper.NearestFreeStation(stations, new Position(18, 12), robots);
            Assert.IsTrue(nearestStationReturned.Count() == 2);
            Assert.AreEqual(nearestStationReturned.First(), nearest);
            Assert.AreEqual(nearestStationReturned.ElementAt(1), secondNearest);
        }

        [TestMethod]
        public void TestIsAtStaion()
        {
            List<EnergyStation> stations = new List<EnergyStation>();
            EnergyStation StWithRobot = new EnergyStation() {Position = new Position(20, 12) };
            stations.Add(new EnergyStation() { Position = new Position(15, 10) });
            stations.Add(StWithRobot);
            stations.Add(new EnergyStation() { Position = new Position(18, 9) });
            stations.Add(new EnergyStation() { Position = new Position(18, 15) });
            EnergyStation returned;
            Assert.IsTrue(Helper.IsAtStation(new Position(18, 12), stations, out returned));
            Assert.IsFalse(Helper.IsAtStation(new Position(28, 12), stations, out _));
            Assert.AreEqual(returned, StWithRobot);
        }
        [TestMethod]
        public void TestFreeLineY()
        {
            //bool FreeLineY(int startPointY, int endPointY, int lineIndexX, IList<Robot.Common.Robot> robots)
            var robots = new List<Robot.Common.Robot>() ;
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 7) });
            Assert.IsTrue(Helper.FreeLineY(10, 15, 15, robots));
            Assert.IsFalse(Helper.FreeLineY(10, 7, 15, robots));
        }
        [TestMethod]
        public void TestFreeLineX()
        {
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 7) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(7, 11) });
            Assert.IsTrue(Helper.FreeLineX(15, 10, 10,robots));
            Assert.IsTrue(Helper.FreeLineX(15, 20, 10, robots));

        }
        [TestMethod]
        public void TestNearestFreeCellAtStation()
        {
            //Position NearestFreeCell(Position stationPosition, Position robotPosition, IList<Robot.Common.Robot> robots)
            Position stationPos = new Position(20, 11),
                robotPosition = new Position(15,10),
                stationPos2 = new Position(10,11);
           var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = robotPosition });
            robots.Add(new Robot.Common.Robot() { Position = new Position(18,10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(12, 11) });
            var nearestPos = Helper.NearestFreeCellAtStation(stationPos, robotPosition, robots);
            Assert.IsTrue(nearestPos.X == 19 && nearestPos.Y == 10 || nearestPos.X == 18 &&( nearestPos.Y == 11||nearestPos.Y==9));

            nearestPos = Helper.NearestFreeCellAtStation(stationPos2, robotPosition, robots);
            Assert.IsTrue(nearestPos.X == 12 && nearestPos.Y == 10 );
        }
        [TestMethod]
        public void TestNearRobotPos()
        {
            // Position NearRobotPos(IEnumerable<EnergyStation> stations,Position robotPosition, IList<Robot.Common.Robot> robots)
            List<EnergyStation> stations = new List<EnergyStation>();
            stations.Add(new EnergyStation() { Position = new Position(13, 10) });
            Position robotPos = new Position(15, 10);
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(16, 10) });
            Position nearCell = Helper.NearRobotFreeCell(stations, robotPos, robots);
            Assert.IsTrue(nearCell.X == 14 && nearCell.Y == 10);

            stations.Clear();
            stations.Add(new EnergyStation() { Position= new Position(15, 7) });
            nearCell = Helper.NearRobotFreeCell(stations, robotPos, robots);
            Assert.IsTrue(nearCell.X == 15 && nearCell.Y == 9);

            stations.Clear();
            stations.Add(new EnergyStation() { Position = new Position(18, 10) });
            nearCell = Helper.NearRobotFreeCell(stations, robotPos, robots);
            Assert.IsTrue(nearCell.X == 16 && nearCell.Y == 11);
        }

        //Position NextMove(IList<EnergyStation> stations, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, out bool WithAtack)
        [TestMethod]
        public void TestNextMove() {
            Position robotPos = new Position(15, 10);
            Robot.Common.Robot currentRobot = new Robot.Common.Robot() { Position = robotPos,Energy = 30 };
            List<EnergyStation> stations = new List<EnergyStation>();
            
            stations.Add(new EnergyStation() { Position = new Position(17, 8) ,Energy = 30});
            stations.Add(new EnergyStation() { Position = new Position(12, 12),Energy = 40 });
            stations.Add(new EnergyStation() { Position = new Position(18, 12), Energy = 400 });
            stations.Add(new EnergyStation() { Position = new Position(15, 14),Energy = 200 });

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(currentRobot);
            robots.Add(new Robot.Common.Robot() { Position = new Position(11,8) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(12, 8) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(13, 8) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(11, 9) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(11, 10) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(13, 11) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 13) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 11) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 12) });
            Position nextPosition = Helper.NextMove(stations, currentRobot, robots, out _);
            Assert.IsTrue(nextPosition.X == 16 && nextPosition.Y == 10);

        }

        [TestMethod]
        public void TestAlmostFreeStAtLine()
        {
            Position robotPos = new Position(15, 10);
            Robot.Common.Robot currentRobot = new Robot.Common.Robot() { Position = robotPos, Energy = 100 };
            List<EnergyStation> stations = new List<EnergyStation>();
            stations.Add(new EnergyStation() { Position = new Position(16, 4),Energy = 500 });
            stations.Add(new EnergyStation() { Position = new Position(14, 6),Energy = 200 });
            stations.Add(new EnergyStation() { Position = new Position(15, 14),Energy = 100 });
            stations.Add(new EnergyStation() { Position = new Position(21, 10),Energy =150 });
            stations.Add(new EnergyStation() { Position = new Position(11, 11),Energy = 20 });

            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = robotPos,Energy = 100 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 12) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 13) });
            robots.Add(new Robot.Common.Robot() { Position = new Position(17, 11) });

            Position stationPos = Helper.AlmostFreeStAtLine(stations,robotPos,robots)?.Position;
            Assert.IsTrue(stationPos.X == 14 && stationPos.Y == 6);

            stations.Clear();
            stations.Add(new EnergyStation() { Position = new Position(23, 23) });
            Assert.IsNull(Helper.AlmostFreeStAtLine(stations, robotPos, robots));


        }

        [TestMethod]
        public void TestCreatingRobot()
        {
            var algorithm = new KishchukAlgorithm();
            var robots = new List<Robot.Common.Robot>();

            robots.Add (new Robot.Common.Robot() { Position = new Position(15, 10),Energy=350 });
            Assert.IsInstanceOfType(algorithm.DoStep(robots, 0, null), typeof(CreateNewRobotCommand));
        }
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new KishchukAlgorithm();
            var robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10), Energy = 150 });
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position= new Position(15, 13) });
            Assert.IsInstanceOfType(algorithm.DoStep(robots, 0, map),typeof(MoveCommand));
            robots.Clear();
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 20) });
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 20),Energy = 10 });
            Assert.IsInstanceOfType(algorithm.DoStep(robots, 0, map), typeof(MoveCommand));


        }
        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new KishchukAlgorithm();
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 10), Energy = 150 });
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(15, 10),Energy =10 });
            
            Assert.IsInstanceOfType(algorithm.DoStep(robots, 0, map), typeof(CollectEnergyCommand));

        }
    }
}
